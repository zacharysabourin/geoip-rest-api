package org.eclipsefoundation.geoip.client.test.namespaces;

public class SchemaNamespaceHelper {
        public static final String BASE_SCHEMAS_PATH = "schemas/";
        public static final String BASE_SCHEMAS_PATH_SUFFIX = "-schema.json";
        public static final String CITY_SCHEMA_PATH = BASE_SCHEMAS_PATH + "city"
                        + BASE_SCHEMAS_PATH_SUFFIX;
        public static final String COUNTRY_SCHEMA_PATH = BASE_SCHEMAS_PATH + "country"
                        + BASE_SCHEMAS_PATH_SUFFIX;
        public static final String IP_ADDRESSES_SCHEMA_PATH = BASE_SCHEMAS_PATH + "ip-addresses"
                        + BASE_SCHEMAS_PATH_SUFFIX;
}
