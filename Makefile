clean:;
	rm -rf ./maxmind
	mvn clean
install: clean;
	yarn install --frozen-lockfile 
	sh ./bin/maxmind.sh maxmind/
package: install;
	mvn compile package
dirty-package:;
	mvn compile package -Declipse.maxmind.root=${PWD}/maxmind
production-build:;
	yarn install --frozen-lockfile
	yarn run generate-json-schema
	mvn compile package -Declipse.maxmind.root=${PWD}/maxmind
headless-docker: package;
	docker-compose down
	docker-compose build
	docker-compose up -d